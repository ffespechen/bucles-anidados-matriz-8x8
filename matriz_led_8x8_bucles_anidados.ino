//Cargar librerías para LCD
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>


#include "LedControl.h"

//Librería: https://github.com/wayoda/LedControl
/*
 pin 12 DataIn 
 pin 11 CLK 
 pin 10 CS 
 Una sola matriz 8x8 MAX72XX.
 */
LedControl lc=LedControl(12,11,10,1);


unsigned long delaytime=500;

const int filas = 8;
const int columnas = 8;


// Parametrización del LCD

//Define variables 

#define I2C_ADDR          0x27        //Define I2C Address where the PCF8574A is
#define BACKLIGHT_PIN      3
#define En_pin             2
#define Rw_pin             1
#define Rs_pin             0
#define D4_pin             4
#define D5_pin             5
#define D6_pin             6
#define D7_pin             7


//Inicializar el LCD
LiquidCrystal_I2C      lcd(I2C_ADDR, En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);


void setup() {
  /*
   The MAX72XX is in power-saving mode on startup,
   we have to do a wakeup call
   */
  lc.shutdown(0,false);
  lc.setIntensity(0,2);
  lc.clearDisplay(0);

  //Establecer si se trata de un LCD 16x2 o 20x4 (caracteres x líneas) 
  lcd.begin (16,2);
  
  //Encender la iluminación de fondo
  lcd.setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd.setBacklight(HIGH);

  //Posicionar el cursor en columa, fila
  lcd.setCursor(1,0);
  lcd.print("Fila:");
  lcd.setCursor(1,1);
  lcd.print("Columna:");
}


void loop() {

  for(int i=0; i<filas; i++)
  {
    for(int j=0;j<columnas; j++)
    {

      lcd.setCursor(9,0);
      lcd.print(i);
      lcd.setCursor(9,1);
      lcd.print(j);
      /*
       * Parámetros:
       * Matriz, fila, columna, estado
       */
      lc.setLed(0, i, j, true);
      delay(delaytime);
    }
  }

  lc.clearDisplay(0);
  delay(delaytime);

}
